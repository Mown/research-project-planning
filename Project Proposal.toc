\select@language {british}
\contentsline {chapter}{Contents}{iii}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{section*.3}
\contentsline {section}{\numberline {1.1}Topic covered by the project}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Keywords}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Problem description}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Justification, motivation and benefits}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}Research questions}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Planned contributions}{3}{section.1.6}
\contentsline {chapter}{\numberline {2}Related work}{5}{section*.4}
\contentsline {chapter}{\numberline {3}Choice of methods}{7}{section*.5}
\contentsline {chapter}{\numberline {4}Milestones, deliverables and resources}{9}{section*.6}
\contentsline {section}{\numberline {4.1}Resources}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Deliverables}{9}{section.4.2}
\contentsline {section}{\numberline {4.3}Milestones}{9}{section.4.3}
\contentsline {chapter}{\numberline {5}Feasibility study}{11}{section*.7}
\contentsline {chapter}{\numberline {6}Risk analysis}{13}{section*.8}
\contentsline {chapter}{\numberline {7}Ethical and legal considerations}{15}{section*.9}
\contentsline {chapter}{Bibliography}{17}{section*.11}
