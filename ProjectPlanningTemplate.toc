\select@language {british}
\contentsline {chapter}{Revision history}{iii}{section*.2}
\contentsline {paragraph}{Control questions}{i}{section*.3}
\contentsline {chapter}{Contents}{iii}{section*.5}
\contentsline {chapter}{\numberline {1}Contents of the project description}{1}{section*.6}
\contentsline {chapter}{\numberline {2}Introduction (1-2 pages)}{3}{section*.7}
\contentsline {section}{\numberline {2.1}Topic covered by the project}{3}{section.2.1}
\contentsline {paragraph}{Control questions:}{3}{section*.8}
\contentsline {section}{\numberline {2.2}Keywords}{3}{section.2.2}
\contentsline {paragraph}{Control questions:}{3}{section*.9}
\contentsline {section}{\numberline {2.3}Problem description}{4}{section.2.3}
\contentsline {paragraph}{Control questions:}{4}{section*.10}
\contentsline {section}{\numberline {2.4}Justification, motivation and benefits}{4}{section.2.4}
\contentsline {paragraph}{Control questions}{4}{section*.11}
\contentsline {section}{\numberline {2.5}Research questions}{4}{section.2.5}
\contentsline {paragraph}{Control questions:}{5}{section*.12}
\contentsline {section}{\numberline {2.6}Planned contributions}{5}{section.2.6}
\contentsline {paragraph}{Control questions}{5}{section*.13}
\contentsline {chapter}{\numberline {3}Related work (3-10 pages)}{7}{section*.14}
\contentsline {section}{\numberline {3.1}Handling Potential problems}{7}{section.3.1}
\contentsline {paragraph}{Question 1}{7}{section*.15}
\contentsline {paragraph}{Answer 1.A}{7}{section*.16}
\contentsline {paragraph}{Answer 1.B}{7}{section*.17}
\contentsline {paragraph}{Question 2}{7}{section*.18}
\contentsline {paragraph}{Answer 2.A}{8}{section*.19}
\contentsline {paragraph}{Answer 2B}{8}{section*.20}
\contentsline {paragraph}{Control questions:}{8}{section*.21}
\contentsline {chapter}{\numberline {4}Choice of methods (2-5 pages)}{9}{section*.22}
\contentsline {chapter}{\numberline {5}Milestones, deliverables and resources (2-5 pages)}{11}{section*.23}
\contentsline {chapter}{\numberline {6}Feasibility study (1/2-3 pages)}{13}{section*.24}
\contentsline {chapter}{\numberline {7}Risk analysis (1/2-2 pages)}{15}{section*.25}
\contentsline {chapter}{\numberline {8}Ethical and legal considerations (1/4-1 page)}{17}{section*.26}
\contentsline {chapter}{\numberline {9}The bibliography}{19}{section*.27}
\contentsline {chapter}{\numberline {A}Known problems with MikTeX and TeXnicCenter}{21}{section*.28}
\contentsline {section}{\numberline {A.1}Hig logo xor url's}{21}{section.A.1}
\contentsline {paragraph}{Problem}{21}{section*.29}
\contentsline {paragraph}{Possible solution 1}{21}{section*.30}
\contentsline {paragraph}{Possible solution 2}{21}{section*.31}
\contentsline {section}{\numberline {A.2}\textlatin {\LaTeX }/AFPL Ghostscript crashes}{21}{section.A.2}
\contentsline {paragraph}{Problem}{21}{section*.32}
\contentsline {paragraph}{Possible solution}{21}{section*.33}
\contentsline {chapter}{Bibliography}{23}{section*.35}
